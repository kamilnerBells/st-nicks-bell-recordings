# St Nicks Bell Recordings

Recordings of bells of St. Nicholas Church, Hurst along with any related packaging and metadata to use them in different programs.

Note ranges and associated MIDI note values are:

```
Bell 1 (treble)  F   MIDI Note 65
Bell 2           E   MIDI Note 64
Bell 3           D   MIDI Note 62
Bell 4           C   MIDI Note 60
Bell 5           Bb  MIDI Note 58
Bell 6           A   MIDI Note 57
Bell 7           G   MIDI Note 55
Bell 8 (tenor)   F   MIDI Note 53
```

## Soundfont file

The soundfont file was created with Swami Instrument Editor and should work with any system that uses Soundfonts.

## Bell Definition File

The Bell Definition File is designed to work with Abel and other Ringing Simulator Packages that use BDF files.

To use the file, the bell samples in the Recordings folder, and the bdf file, need to be copied into the Abel 3\Bells subdirectory.
See http://www.abelsim.co.uk/doc/bellsnd3.htm for details.


